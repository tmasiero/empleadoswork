<html>
    <head>
        <meta charset="UTF-8">
        <title>Personas y subir archivos</title>
        <link rel="stylesheet" href="Empleados/css/style.css" type="text/css">
        <link rel="stylesheet" type="text/css" href="Empleados/css/dropzone.css">
        <link href="https://fonts.googleapis.com/css?family=Margarine" rel="stylesheet"> 
    </head>
    <script src="Empleados/js/jquery_v3.3.1.js"></script>
    <script src="Empleados/js/dropzone.js"></script>
    <script src="Empleados/js/ajax.js"></script>
    <body>
        <div class="bg-amarillo contenedor sombra">
            <form class="inscribir widget" id="inscribir" style="display:none;">
                <div id="añadir" style="display:none;">
                    <legend>Añada un contacto <span>Todos los campos son obligatorios</span></legend>
                </div>               
                    <div class="campos">
                        <div class="campo">
                            <label for="nombre">Nombre:</label>
                            <input type="text" name="nombre" id="nombre" placeholder="NOMBRE" required>
                        </div>
                        <div class="campo">
                            <label for="nombre">Email:</label>
                            <input type="text" name="email" id="email" placeholder="E-MAIL" required>
                        </div>
                        <div class="campo">
                            <label for="nombre">Telefono:</label>
                            <input type="tel" name="telefono" id="telefono" placeholder="TELEFONO" required>
                        </div>
                        <div class="campo">
                            <label for="nombre">Comentario:</label>
                            <input type="text" name="comentario" id="comentario" placeholder="Escriba comentario corto" required>
                        </div>
                        <div class="campo">
                            <label for="nombre">Nombre Archivo:</label>
                            <input type="text" name="nombre_archivo" id="nombre_archivo" placeholder="Escriba nombre del archivo" required>
                        </div>
                    </div>
                <div class="campo enviar">
                        <input type="submit" id="envio" value="Añadir">
                        <a class="btn volver">Volver</a>
                </div>
                <div id="data-editar"></div>
            </form>
            <section id="consultar" class="widget">
                <div class="contenedor-barra">
                    <h4 class="widgettitulo">Listado de Personas</h4>
                </div>
                <div class="datagrid" id="datagrid"></div> 
                <button class="volver" onclick="nueva_persona()">Nueva persona</button>
                <!--<button class="volver" onclick="guardar()">Guardar tabla</button>-->
            </section>
            <form class="inscribir widget" id="form_editar" style="display:none;">
                <div id="editar_encabezado" style="display:none;">
                    <legend>Edite un contacto <span>Todos los campos son obligatorios</span></legend>
                </div>
                    <div class="campos">
                        <input type="hidden" id="id_editar" name="id" />
                        <div class="campo">
                            <label for="nombre">Nombre:</label>
                            <input type="text" name="nombre" id="nombre_editar" >
                        </div>
                        <div class="campo">
                            <label for="nombre">Email:</label>
                            <input type="text" name="email" id="email_editar" >
                        </div>
                        <div class="campo">
                            <label for="nombre">Telefono:</label>
                            <input type="tel" name="telefono" id="telefono_editar" >
                        </div>
                        <div class="campo">
                            <label for="nombre">Comentario:</label>
                            <input type="text" name="comentario" id="comentario_editar" >
                        </div>
                        <div class="campo">
                            <label for="nombre">Nombre Archivo:</label>
                            <input type="text" name="nombre_archivo" id="nombre_archivo_editar" >
                        </div>
                    </div>
                <div class="campo enviar">
                        <input type="submit" id="editando" value="Editar">
                        <a class="btn volver">Volver</a>
                </div>
                <div class="datagrid" id="mostrar_editar"></div>
            </form>
            <form class="dropzone" id="myDrop" action="upload.php">
            
            </form>
<!--            <form class="dropzone" id="myDrop" style="display:none;">
                <input type="file" name="file">
            </form>-->
        </div>
        
    </body>
</html>
